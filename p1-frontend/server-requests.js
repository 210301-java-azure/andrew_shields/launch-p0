function sendAjaxRequest(method, url, body, successCallback, failureCallback, username, authToken){
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open(method, url); // ready state 1
    // let token = sessionStorage.getItem("jwt"); // we could set the Auth header with this value instead
    if(authToken){
        xhr.setRequestHeader("Authorization", authToken);
    }

    if(username){
        xhr.setRequestHeader("Username", username);
    }
    
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status < 300 && xhr.status > 199){
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    }else {
        xhr.send(); // ready state 2,3,4 follow
    }
     
}

function sendAjaxGet(url, body, successCallback, failureCallback, username, authToken){
    sendAjaxRequest("GET", url, body, successCallback, failureCallback, username, authToken);
}
function sendAjaxPost(url, body, successCallback, failureCallback, username, authToken){
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, username, authToken);
}


// User Functions -- no authorization required

function ajaxLogin(username, password, successCallback, failureCallback){
    const payload = `username=${username}&password=${password}`;
    sessionStorage.setItem("Username", username);
    sendAjaxPost("http://104.43.234.243:80/login", payload, successCallback, failureCallback);

}

function ajaxCreateClient(client, successCallback, failureCallback){
    const itemJson = JSON.stringify(client);
    sendAjaxPost("http://104.43.234.243:80/users", itemJson, successCallback, failureCallback);
}

function ajaxPullUserData(successCallback, failureCallback){
    const user = sessionStorage.getItem("Username");
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/users", undefined, successCallback, failureCallback, user, auth);
}

function ajaxPullUserInfoData(clientId, successCallback, failureCallback, username){
    sendAjaxGet(`http://104.43.234.243:80/users/info/${clientId}`, undefined, successCallback, failureCallback, username);
}

function ajaxPostUserInfo(clientId, body, successCallback, failureCallback){
    const payload = JSON.stringify(body);
    sendAjaxPost(`http://104.43.234.243:80/users/info/${clientId}`, payload, successCallback, failureCallback);
}



// Admin Functions -- JWT authorization required

function ajaxAdminPullAllUsers(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/admins/users", undefined, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminPullAllAdmins(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/admins/admins", undefined, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminPullAllCommanders(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/admins/commanders", undefined, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminPullAllPassengers(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/admins/passengers", undefined, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminPullAllRockets(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/admins/rockets", undefined, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminPullAllLaunches(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    sendAjaxGet("http://104.43.234.243:80/admins/launches", undefined, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminPostNewRocket(rocketInfo, successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    const payload = JSON.stringify(rocketInfo);
    sendAjaxPost("http://104.43.234.243:80/admins/rockets", payload, successCallback, failureCallback, undefined, auth);
}

function ajaxAdminAssignRocket(rocketId, launchId, successCallback, failureCallback){
    const payload = `rocket=${rocketId}&launch=${launchId}`;
    const auth = sessionStorage.getItem("JWT");
    sendAjaxPost("http://104.43.234.243:80/admins/launches/rocket", payload, successCallback, failureCallback, undefined, auth);
}



// Commander Functions -- requires authentication

function ajaxPostNewLaunch(launchInfo, successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    const payload = JSON.stringify(launchInfo);
    sendAjaxPost("http://104.43.234.243:80/commanders/launches", payload, successCallback, failureCallback, undefined, auth);
}


// Passenger Functions -- requries authentication

function ajaxBookLaunch(launch, successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    const id = sessionStorage.getItem("ID");

    sendAjaxPost(`http://104.43.234.243:80/passengers/${id}`, launch, successCallback, failureCallback, undefined, auth);
}

function ajaxGetPassengerInfo(successCallback, failureCallback){
    const auth = sessionStorage.getItem("JWT");
    const usn = sessionStorage.getItem("Username");
    sendAjaxGet(`http://104.43.234.243:80/passengers/${usn}`, undefined, successCallback, failureCallback, undefined, auth);
}