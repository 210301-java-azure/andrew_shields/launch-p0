const token = sessionStorage.getItem("JWT");

if(token!=null){
    document.getElementById("login-nav-item").hidden=true;
    document.getElementById("logout-nav-item").hidden=false;
    document.getElementById("account-nav-item").hidden=false;
}

document.getElementById("logout-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("JWT");
    sessionStorage.removeItem("ID");
    sessionStorage.removeItem("Username");
    document.getElementById("login-nav-item").hidden=false;
    document.getElementById("logout-nav-item").hidden=true;
    document.getElementById("account-nav-item").hidden=true;
    window.location.href="./home.html";
}