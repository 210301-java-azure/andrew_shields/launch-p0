function checkID(){
    const id = sessionStorage.getItem("ID");
    const user = sessionStorage.getItem("Username");

    if (id==null || id==undefined){
        if(user){
            ajaxPullUserData(successClientData, failureClientData);
        }
        
    }
}

function successClientData(xhr){

    const clientData = JSON.parse(xhr.responseText);

    sessionStorage.setItem("ID", clientData.id);

}

function failureClientData(){
    console.log("Server problem retrieving ID");
}