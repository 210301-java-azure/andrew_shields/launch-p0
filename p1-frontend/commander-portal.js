document.getElementById("logout-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("JWT");
    sessionStorage.removeItem("ID");
    sessionStorage.removeItem("Username");
    window.location.href="./home.html";
}

// Commander launch creation function

document.getElementById("create-launch-btn").addEventListener("click", openLaunchForm);

function openLaunchForm(){
    document.getElementById("launch-info-form").hidden=false;
}

document.getElementById("launch-info-form").addEventListener("submit", postNewLaunch);

function postNewLaunch(event){
    event.preventDefault();
    
    const loc = document.getElementById("location").value;
    const dest = document.getElementById("destination").value;
    const lDate = document.getElementById("launchDate").value;
    const lTime = document.getElementById("launchTime").value;
    const aDate = document.getElementById("arrivalDate").value;
    const aTime = document.getElementById("arrivalTime").value;

    const data = {launchDate: lDate, launchTime: lTime, launchLocation: loc, launchDestination: dest, arrivalDate: aDate, arrivalTime: aTime};

    ajaxPostNewLaunch(data, successPostLaunch, failurePostLaunch);

}
function successPostLaunch(){
    const successDiv = document.getElementById("success-msg");
    successDiv.hidden=false;
    successDiv.innerText = "Launch has been stored successfully!";
}
function failurePostLaunch(xhr){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden=false;
    errorDiv.innerText = xhr.responseText;
}