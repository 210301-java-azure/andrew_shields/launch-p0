function checkValue(val){
    if(val=="Passenger"){
        document.getElementById("fullName").disabled=false;
        document.getElementById("commanderName").disabled=true;
        document.getElementById("commanderLaunchSite").disabled=true;
        document.getElementById("adminAllocation").disabled=true;
    }
    if(val=="Commander"){
        document.getElementById("commanderName").disabled=false;
        document.getElementById("commanderLaunchSite").disabled=false;
        document.getElementById("fullName").disabled=true;
        document.getElementById("adminAllocation").disabled=true;
    }
    if(val=="Administrator"){
        document.getElementById("fullName").disabled=false;
        document.getElementById("adminAllocation").disabled=false;
        document.getElementById("commanderName").disabled=true;
        document.getElementById("commanderLaunchSite").disabled=true;
    }
}

document.getElementById("new-client-form").addEventListener("submit", createAccount);

function createAccount(event){
    event.preventDefault();

    const username = document.getElementById("username").value;
    const role = document.getElementById("role").value;
    const password = document.getElementById("password").value;
    const password2 = document.getElementById("password2").value;
    if(password!=password2){
        const message = document.getElementById("password-error");
        message.hidden=false;
        message.innerText = "Passwords do not match!";
    }
    if(role=="Passenger"){
        const fullName = document.getElementById("fullName").value;
        const client = {username: username, password: password, role: role, name: fullName};
        ajaxCreateClient(client, indicateSuccess, indicateFailure);
    }
    if(role=="Commander"){
        const fullName = document.getElementById("commanderName").value;
        const launchSite = document.getElementById("commanderLaunchSite").value;
        const client = {username: username, password: password, role: role, name: fullName, launch_site: launchSite};
        ajaxCreateClient(client, indicateSuccess, indicateFailure);
    }
    if(role=="Administrator"){
        const fullName = document.getElementById("fullName").value;
        const alloc = document.getElementById("adminAllocation").value;
        const client = {username: username, password: password, role: role, name: fullName, allocation: alloc};
        ajaxCreateClient(client, indicateSuccess, indicateFailure);
    }
}

function indicateSuccess(){
    window.location.href="./login.html";
}

function indicateFailure(){
    const message = document.getElementById("password-error");
        message.hidden=false;
        message.innerText = "Could not create new account! *cries*";
}