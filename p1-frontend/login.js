document.getElementById("login-form").addEventListener("submit", attemptLogin);



function attemptLogin(event){
    event.preventDefault(); // this stops the form from sending an GET request by default to the same URL
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    ajaxLogin(username, password, successfulLogin, loginFailed);

}

function successfulLogin(xhr){
    const authToken = xhr.responseText;
    sessionStorage.setItem("JWT", authToken);
    window.location.href="./home.html";
}

function loginFailed(xhr){

    //console.log("Oh no! Smething went terribly wrong!");

    const errorDiv = document.getElementById("error-msg")
    errorDiv.hidden=false;
    errorDiv.innerText = xhr.responseText;

}
