
const auth = sessionStorage.getItem("JWT");

// getting information from the server

const xhr = new XMLHttpRequest();
xhr.open("GET", "http://104.43.234.243:80/passengers/launches");
xhr.setRequestHeader("Authorization", auth);
xhr.onreadystatechange = function(){
    if(this.readyState==4){
        if(xhr.status==200){
            const launches = JSON.parse(xhr.responseText);
            renderLaunchesInTable(launches);
            renderLaunchIDinSelect(launches);
        } else {
            displayError();
        }
    }
}
xhr.send();

function displayError(){
    document.getElementById("error-msg-table").hidden=false;
    const msg = document.getElementById("error-msg-table");
    msg.innerText = "You must be a Passenger to view launches!";
}


function renderLaunchesInTable(launches){

    document.getElementById("launches").hidden=false;
    const tableBody = document.getElementById("launches-body");

    for(let launch of launches){
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${launch.launchId}</td><td>${launch.launchDate}</td><td>${launch.launchTime}</td><td>${launch.launchLocation}</td><td>${launch.launchDestination}</td><td>${launch.arrivalDate}</td><td>${launch.arrivalTime}</td>`;
        tableBody.appendChild(newRow);
    }

}

function renderLaunchIDinSelect(launches){

    document.getElementById("launches-select").disabled=false;
    const selectBody = document.getElementById("launches-select")

    for(let launch of launches){
        let newOption = document.createElement("option");
        newOption.value = launch.launchId;
        newOption.innerText = `${launch.launchId}`;
        selectBody.appendChild(newOption);
    }

}


document.getElementById("book-launch-btn").addEventListener("click", passengerBookLaunch);

function passengerBookLaunch(){
    const launchOption = document.getElementById("launches-select").value;
    const launchForm = `launch=${launchOption}`;

    ajaxBookLaunch(launchForm, successBook, failureBook);

    function successBook(){
        document.getElementById("success-msg").hidden=false;
        const msg = document.getElementById("success-msg");
        msg.innerText = "You have succcessfully booked this launch!"
    }

    function failureBook(){
        document.getElementById("error-msg").hidden=false;
        const msg = document.getElementById("error-msg");
        msg.innerText = "Failed to book this launch! -- If there is no rocket assigned you cannot book the launch!";
    }
}

function showPassengerInfo(){
    ajaxGetPassengerInfo(successGetInfo, failureGetInfo);
    function successGetInfo(xhr1){
        const info = xhr1.responseText;
        const launch = info.launchId;
        const tableBody = document.getElementById("info-body");
        let newRow = document.createElement("tr");
        newRow.innerHTML = `<td>${info.passengerId}</td><td>${info.name}</td><td>${info.seatNo}</td><td>${info.travelClass}</td><td>${launch.launchLocation}</td><td>${launch.launchDate}</td><td>${launch.launchTime}</td>`;
        tableBody.appendChild(newRow);
    }
    function failureGetInfo(xhr1){
        document.getElementById("error-msg").hidden=false;
        const msg = document.getElementById("error-msg");
        msg.innerText = xhr1.responseText;
    }
}



