document.getElementById("logout-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("JWT");
    sessionStorage.removeItem("ID");
    sessionStorage.removeItem("Username");
    window.location.href="./home.html";
}

// Admin rocket assignment function

document.getElementById("assignment-btn").addEventListener("click", adminAssignment);

function adminAssignment(){
    document.getElementById("rocket-assignment-form").hidden=false;
    document.getElementById("data-selector").hidden=true;
}

document.getElementById("rocket-assignment-form").addEventListener("submit", assignRocket);

function assignRocket(event){
    event.preventDefault();
    
    const rocket = document.getElementById("rocketId").value;
    const launch = document.getElementById("launchId").value;

    ajaxAdminAssignRocket(rocket, launch, successAssignment, failureAssignment);

}

function successAssignment(){
    const successDiv = document.getElementById("success-msg");
    successDiv.hidden=false;
    successDiv.innerText = "Rocket has been assigned successfully!";
}

function failureAssignment(xhr){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden=false;
    errorDiv.innerText = xhr.responseText;
}


// Admin rocket creation function

document.getElementById("create-rocket-btn").addEventListener("click", openRocketForm);

function openRocketForm(){
    document.getElementById("data-selector").hidden=true;
    document.getElementById("rocket-assignment-form").hidden=true;
    dataTable.hidden=true;
    document.getElementById("rocket-info-form").hidden=false;
}

document.getElementById("rocket-info-form").addEventListener("submit", postNewRocket);

function postNewRocket(event){
    event.preventDefault();
    const name = document.getElementById("rocketName").value;
    const count = document.getElementById("seatCount").value;
    const avail = document.getElementById("seatsAvail").value;
    const filled = document.getElementById("seatsFilled").value;

    const rocketInfo = {rocketName: name, seatCount: count, seatsAvail: avail, seatsFilled: filled};

    ajaxAdminPostNewRocket(rocketInfo, successPostRocket, failurePostRocket);

    

}
function successPostRocket(){
    const successDiv = document.getElementById("success-msg");
    successDiv.hidden=false;
    successDiv.innerText = "Rocket has been stored successfully!";
}
function failurePostRocket(xhr){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden=false;
    errorDiv.innerText = xhr.responseText;
}

// Database GET functions for admins

document.getElementById("data-view-btn").addEventListener("click", displayViewOption);

function displayViewOption(){
    document.getElementById("rocket-info-form").hidden=true;
    document.getElementById("data-selector").hidden=false;
    document.getElementById("rocket-assignment-form").hidden=true;
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

const dataTable = document.getElementById("data-table");
const head = document.createElement("thead");
const tableBody = document.createElement("tbody");

function checkValue(val){
    function failurePull(xhr){
        const errorDiv = document.getElementById("error-msg");
        errorDiv.hidden=false;
        errorDiv.innerText = xhr.responseText;
    }
    if(val=="1"){
        if(head.hasChildNodes){
            removeAllChildNodes(head);
        }
        if(tableBody.hasChildNodes){
            removeAllChildNodes(tableBody);
        }
        if(dataTable.hasChildNodes){
            removeAllChildNodes(dataTable);
        }

        document.getElementById("data-table").hidden=false;

        const colHeaders = document.createElement("tr");
        let newCol0 = document.createElement("th");
        newCol0.scope = "col";
        newCol0.innerText = "ID";
        colHeaders.appendChild(newCol0);
        let newCol = document.createElement("th");
        newCol.scope = "col";
        newCol.innerText = "Username";
        colHeaders.appendChild(newCol);
        let newCol1 = document.createElement("th");
        newCol1.scope = "col";
        newCol1.innerText = "Role";
        colHeaders.appendChild(newCol1);

        head.appendChild(colHeaders);

        dataTable.appendChild(head);

        ajaxAdminPullAllUsers(successPullUsers, failurePull);
        function successPullUsers(xhr){
            const clients = JSON.parse(xhr.responseText);

            for(let client of clients){

                let newRow = document.createElement("tr");
                let newID = document.createElement("td");
                newID.innerText = client.id;
                newRow.appendChild(newID);
                let newUsn = document.createElement("td");
                newUsn.innerText = client.username;
                newRow.appendChild(newUsn);
                let newRole = document.createElement("td");
                newRole.innerText = client.type;
                newRow.appendChild(newRole);

                tableBody.appendChild(newRow);
            }

            dataTable.appendChild(tableBody);

        }
    }
    if(val=="2"){
        if(head.hasChildNodes){
            removeAllChildNodes(head);
        }
        if(tableBody.hasChildNodes){
            removeAllChildNodes(tableBody);
        }
        if(dataTable.hasChildNodes){
            removeAllChildNodes(dataTable);
        }


        document.getElementById("data-table").hidden=false;

        const colHeaders = document.createElement("tr");
        let newCol0 = document.createElement("th");
        newCol0.scope = "col";
        newCol0.innerText = "ID";
        colHeaders.appendChild(newCol0);
        let newCol = document.createElement("th");
        newCol.scope = "col";
        newCol.innerText = "Name";
        colHeaders.appendChild(newCol);
        let newCol1 = document.createElement("th");
        newCol1.scope = "col";
        newCol1.innerText = "Allocation";
        colHeaders.appendChild(newCol1);

        head.appendChild(colHeaders);

        dataTable.appendChild(head);


        ajaxAdminPullAllAdmins(successPullAdmins, failurePull);
        function successPullAdmins(xhr){
            const admins = JSON.parse(xhr.responseText);

            for(let admin of admins){
                let newRow = document.createElement("tr");
                let newID = document.createElement("td");
                newID.innerText = admin.adminId;
                newRow.appendChild(newID);
                let newName = document.createElement("td");
                newName.innerText = admin.name;
                newRow.appendChild(newName);
                let newAlloc = document.createElement("td");
                newAlloc.innerText = admin.allocation;
                newRow.appendChild(newAlloc);

                tableBody.appendChild(newRow);
            }

            dataTable.appendChild(tableBody);

        }
    }
    if(val=="3"){
        if(head.hasChildNodes){
            removeAllChildNodes(head);
        }
        if(tableBody.hasChildNodes){
            removeAllChildNodes(tableBody);
        }
        if(dataTable.hasChildNodes){
            removeAllChildNodes(dataTable);
        }

        document.getElementById("data-table").hidden=false;

        const colHeaders = document.createElement("tr");
        let newCol0 = document.createElement("th");
        newCol0.scope = "col";
        newCol0.innerText = "ID";
        colHeaders.appendChild(newCol0);
        let newCol = document.createElement("th");
        newCol.scope = "col";
        newCol.innerText = "Name";
        colHeaders.appendChild(newCol);
        let newCol1 = document.createElement("th");
        newCol1.scope = "col";
        newCol1.innerText = "Launch Site";
        colHeaders.appendChild(newCol1);

        head.appendChild(colHeaders);

        dataTable.appendChild(head);

        ajaxAdminPullAllCommanders(successPullCommanders, failurePull);
        function successPullCommanders(xhr){
            const commanders = JSON.parse(xhr.responseText);

            for(let commander of commanders){
                let newRow = document.createElement("tr");
                let newID = document.createElement("td");
                newID.innerText = commander.id;
                newRow.appendChild(newID);
                let newName = document.createElement("td");
                newName.innerText = commander.name;
                newRow.appendChild(newName);
                let newSite = document.createElement("td");
                newSite.innerText = commander.launchSite;
                newRow.appendChild(newSite);

                tableBody.appendChild(newRow);
            }

            dataTable.appendChild(tableBody);

        }
    }

    if(val=="4"){
        if(head.hasChildNodes){
            removeAllChildNodes(head);
        }
        if(tableBody.hasChildNodes){
            removeAllChildNodes(tableBody);
        }
        if(dataTable.hasChildNodes){
            removeAllChildNodes(dataTable);
        }

        document.getElementById("data-table").hidden=false;

        const colHeaders = document.createElement("tr");
        let newCol0 = document.createElement("th");
        newCol0.scope = "col";
        newCol0.innerText = "ID";
        colHeaders.appendChild(newCol0);
        let newCol = document.createElement("th");
        newCol.scope = "col";
        newCol.innerText = "Name";
        colHeaders.appendChild(newCol);
        let newCol1 = document.createElement("th");
        newCol1.scope = "col";
        newCol1.innerText = "Seat Number";
        colHeaders.appendChild(newCol1);
        let newCol2 = document.createElement("th");
        newCol2.scope = "col";
        newCol2.innerText = "Travel Class";
        colHeaders.appendChild(newCol2);
        let newCol3 = document.createElement("th");
        newCol3.scope = "col";
        newCol3.innerText = "Launch Booked";
        colHeaders.appendChild(newCol3);

        head.appendChild(colHeaders);

        dataTable.appendChild(head);

        ajaxAdminPullAllPassengers(successPullPassengers, failurePull);
        function successPullPassengers(xhr){
            const passengers = JSON.parse(xhr.responseText);

            for(let passenger of passengers){
                let newRow = document.createElement("tr");
                let newID = document.createElement("td");
                newID.innerText = passenger.passengerId;
                newRow.appendChild(newID);
                let newName = document.createElement("td");
                newName.innerText = passenger.name;
                newRow.appendChild(newName);
                let newSeat = document.createElement("td");
                newSeat.innerText = passenger.seatNo;
                newRow.appendChild(newSeat);
                let newClass = document.createElement("td");
                newClass.innerText = passenger.travelClass;
                newRow.appendChild(newClass);
                let newLaunch = document.createElement("td");
                if(passenger.launchId){
                    newLaunch.innerText = "Booked";
                }else{
                    newLaunch.innerText = passenger.launchId;
                }
                
                newRow.appendChild(newLaunch);

                tableBody.appendChild(newRow);
            }

            dataTable.appendChild(tableBody);

        } 
    }

    if(val=="5"){
        if(head.hasChildNodes){
            removeAllChildNodes(head);
        }
        if(tableBody.hasChildNodes){
            removeAllChildNodes(tableBody);
        }
        if(dataTable.hasChildNodes){
            removeAllChildNodes(dataTable);
        }

        document.getElementById("data-table").hidden=false;

        const colHeaders = document.createElement("tr");
        let newCol0 = document.createElement("th");
        newCol0.scope = "col";
        newCol0.innerText = "ID";
        colHeaders.appendChild(newCol0);
        let newCol = document.createElement("th");
        newCol.scope = "col";
        newCol.innerText = "Rocket Name";
        colHeaders.appendChild(newCol);
        let newCol1 = document.createElement("th");
        newCol1.scope = "col";
        newCol1.innerText = "Seat Count";
        colHeaders.appendChild(newCol1);
        let newCol2 = document.createElement("th");
        newCol2.scope = "col";
        newCol2.innerText = "Seats Available";
        colHeaders.appendChild(newCol2);
        let newCol3 = document.createElement("th");
        newCol3.scope = "col";
        newCol3.innerText = "Seats Filled";
        colHeaders.appendChild(newCol3);
        let newCol4 = document.createElement("th");
        newCol4.scope = "col";
        newCol4.innerText = "Commander";
        colHeaders.appendChild(newCol4);

        head.appendChild(colHeaders);

        dataTable.appendChild(head);

        ajaxAdminPullAllRockets(successPullRockets, failurePull);
        function successPullRockets(xhr){
            const rockets = JSON.parse(xhr.responseText);

            for(let rocket of rockets){
                let newRow = document.createElement("tr");
                let newID = document.createElement("td");
                newID.innerText = rocket.rocketId;
                newRow.appendChild(newID);
                let newName = document.createElement("td");
                newName.innerText = rocket.rocketName;
                newRow.appendChild(newName);
                let newSeat = document.createElement("td");
                newSeat.innerText = rocket.seatCount;
                newRow.appendChild(newSeat);
                let newAvail = document.createElement("td");
                newAvail.innerText = rocket.seatsAvail;
                newRow.appendChild(newAvail);
                let newFill = document.createElement("td");
                newFill.innerText = rocket.seatsFilled;
                newRow.appendChild(newFill);
                let newPilot = document.createElement("td");
                newPilot.innerText = rocket.commander;
                newRow.appendChild(newPilot);

                tableBody.appendChild(newRow);
            }

            dataTable.appendChild(tableBody);

        }
    }

    if(val=="6"){
        if(head.hasChildNodes){
            removeAllChildNodes(head);
        }
        if(tableBody.hasChildNodes){
            removeAllChildNodes(tableBody);
        }
        if(dataTable.hasChildNodes){
            removeAllChildNodes(dataTable);
        }

        document.getElementById("data-table").hidden=false;

        const colHeaders = document.createElement("tr");
        let newCol0 = document.createElement("th");
        newCol0.scope = "col";
        newCol0.innerText = "ID";
        colHeaders.appendChild(newCol0);
        let newCol = document.createElement("th");
        newCol.scope = "col";
        newCol.innerText = "Location";
        colHeaders.appendChild(newCol);
        let newCol1 = document.createElement("th");
        newCol1.scope = "col";
        newCol1.innerText = "Destination";
        colHeaders.appendChild(newCol1);
        let newCol2 = document.createElement("th");
        newCol2.scope = "col";
        newCol2.innerText = "Launch Date";
        colHeaders.appendChild(newCol2);
        let newCol3 = document.createElement("th");
        newCol3.scope = "col";
        newCol3.innerText = "Launch Time";
        colHeaders.appendChild(newCol3);
        let newCol4 = document.createElement("th");
        newCol4.scope = "col";
        newCol4.innerText = "Arrival Date";
        colHeaders.appendChild(newCol4);
        let newCol5 = document.createElement("th");
        newCol5.scope = "col";
        newCol5.innerText = "Arrival Time";
        colHeaders.appendChild(newCol5);
        let newCol6 = document.createElement("th");
        newCol6.scope = "col";
        newCol6.innerText = "Rocket";
        colHeaders.appendChild(newCol6);

        head.appendChild(colHeaders);

        dataTable.appendChild(head);

        ajaxAdminPullAllLaunches(successPullLaunches, failurePull);
        function successPullLaunches(xhr){
            const launches = JSON.parse(xhr.responseText);

            for(let launch of launches){
                let newRow = document.createElement("tr");
                let newID = document.createElement("td");
                newID.innerText = launch.launchId;
                newRow.appendChild(newID);
                let newName = document.createElement("td");
                newName.innerText = launch.launchLocation;
                newRow.appendChild(newName);
                let newSeat = document.createElement("td");
                newSeat.innerText = launch.launchDestination;
                newRow.appendChild(newSeat);
                let newAvail = document.createElement("td");
                newAvail.innerText = launch.launchDate;
                newRow.appendChild(newAvail);
                let newFill = document.createElement("td");
                newFill.innerText = launch.launchTime;
                newRow.appendChild(newFill);
                let newData = document.createElement("td");
                newData.innerText = launch.arrivalDate;
                newRow.appendChild(newData);
                let newData1 = document.createElement("td");
                newData1.innerText = launch.arrivalTime;
                newRow.appendChild(newData1);
                let newData2 = document.createElement("td");
                if(launch.rocket){
                    newData2.innerText = "Assigned"
                } else {
                    newData2.innerText = launch.rocket;
                }
                
                newRow.appendChild(newData2);

                tableBody.appendChild(newRow);
            }

            dataTable.appendChild(tableBody);

        }
    }
}


