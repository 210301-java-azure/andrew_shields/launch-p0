document.getElementById("add-info").addEventListener("click", displayInfoForm);

function displayInfoForm(){
    document.getElementById("client-info-form").hidden=false;
    document.getElementById("info-select").hidden=true;
    document.getElementById("fullName").disabled=false;
    document.getElementById("emailInput").disabled=false;
    document.getElementById("phone-num").disabled=false;
    document.getElementById("addressInput").disabled=false;
    document.getElementById("weightInput").disabled=false;
    document.getElementById("ageInput").disabled=false;
    document.getElementById("user-info-submit").disabled=false;
}

document.getElementById("update-info").addEventListener("click", displaySelectorInfo);

function displaySelectorInfo(){
    document.getElementById("info-select").hidden=false;
    document.getElementById("client-info-form").hidden=false;

}

function updateSelect(val){
    if(val=="name"){
        document.getElementById("fullName").disabled=false;
        document.getElementById("emailInput").disabled=true;
        document.getElementById("phone-num").disabled=true;
        document.getElementById("addressInput").disabled=true;
        document.getElementById("weightInput").disabled=true;
        document.getElementById("ageInput").disabled=true;
    }
    if(val=="email"){
        document.getElementById("fullName").disabled=true;
        document.getElementById("emailInput").disabled=false;
        document.getElementById("phone-num").disabled=true;
        document.getElementById("addressInput").disabled=true;
        document.getElementById("weightInput").disabled=true;
        document.getElementById("ageInput").disabled=true;
    }
    if(val=="phone"){
        document.getElementById("fullName").disabled=true;
        document.getElementById("emailInput").disabled=true;
        document.getElementById("phone-num").disabled=false;
        document.getElementById("addressInput").disabled=true;
        document.getElementById("weightInput").disabled=true;
        document.getElementById("ageInput").disabled=true;
    }
    if(val=="address"){
        document.getElementById("fullName").disabled=true;
        document.getElementById("emailInput").disabled=true;
        document.getElementById("phone-num").disabled=true;
        document.getElementById("addressInput").disabled=false;
        document.getElementById("weightInput").disabled=true;
        document.getElementById("ageInput").disabled=true;
    }
    if(val=="weight"){
        document.getElementById("fullName").disabled=true;
        document.getElementById("emailInput").disabled=true;
        document.getElementById("phone-num").disabled=true;
        document.getElementById("addressInput").disabled=true;
        document.getElementById("weightInput").disabled=false;
        document.getElementById("ageInput").disabled=true;
    }
    if(val=="age"){
        document.getElementById("fullName").disabled=true;
        document.getElementById("emailInput").disabled=true;
        document.getElementById("phone-num").disabled=true;
        document.getElementById("addressInput").disabled=true;
        document.getElementById("weightInput").disabled=true;
        document.getElementById("ageInput").disabled=false;
    }
    document.getElementById("user-info-submit").disabled=false;   
}


const usn = document.getElementById("username");
const role = document.getElementById("role");
const naim = document.getElementById("name");
const eMail = document.getElementById("email");
const phone = document.getElementById("phone-number");
const addy = document.getElementById("address");
const wate = document.getElementById("weight");
const dob = document.getElementById("age");


document.getElementById("logout-nav-item").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("JWT");
    sessionStorage.removeItem("ID");
    sessionStorage.removeItem("Username");
    window.location.href="./home.html";
}


document.getElementById("client-info-form").addEventListener("submit", postUserInfo);


function postUserInfo(event){
    event.preventDefault();

    const id = sessionStorage.getItem("ID");
    const fullName = document.getElementById("fullName").value;
    const email = document.getElementById("emailInput").value;
    const phoneNum = document.getElementById("phone-num").value;
    const address = document.getElementById("addressInput").value;
    const weight = document.getElementById("weightInput").value;
    const age = document.getElementById("ageInput").value;
    const clientInfo = {name:fullName, email:email, address:address, phoneNum:phoneNum, weight:weight, age:age};

    ajaxPostUserInfo(id, clientInfo, successPostInfo, failurePostInfo);


}



function pullClientInfo(){

    ajaxPullUserData(successClientData, failureClientData);
    const id = sessionStorage.getItem("ID");
    const user = sessionStorage.getItem("Username");
    ajaxPullUserInfoData(id, successPullInfo, failurePullInfo, user);
}


function successClientData(xhr){

    const clientData = JSON.parse(xhr.responseText);


    usn.innerText = clientData.username;
    role.innerText = clientData.type;

    if(clientData.type=="Administrator"){
        document.getElementById("admin-portal").hidden=false;
    }
    
    if(clientData.type=="Commander"){
        document.getElementById("pilot-portal").hidden=false;
    }

}

function failureClientData(){
    document.getElementById("message-error").hidden=false;
    const message = document.getElementById("message-error");
    message.innerText = "Could not retrieve client ID from session.";
}


function successPostInfo(){
    document.getElementById("password-error").hidden=false;
    const message = document.getElementById("password-error");
    message.innerText = "Information posted successfully!";

}

function failurePostInfo(){
    document.getElementById("password-error").hidden=false;
    const message = document.getElementById("password-error");
    message.innerText = "Could not post client information to the database.";
}

function successPullInfo(xhr){
    const clientInfo = JSON.parse(xhr.responseText);
    naim.innerText = clientInfo.name;
    eMail.innerText = clientInfo.email;
    addy.innerText = clientInfo.address;
    phone.innerText = clientInfo.phoneNum;
    wate.innerText = clientInfo.weight;
    dob.innerText = clientInfo.age;
}

function failurePullInfo(){
    document.getElementById("message-error").hidden=false;
    const message = document.getElementById("message-error");
    message.innerText = "Could not pull client information from the database.";
}