Beyond Earth
A web application that aims to bring travel commerce to the space sector and take humanity beyond Earth.

Technologies Used

    Javalin - version 3.13.3
    JWT API - version 0.11.2
    Hibernate - version 5.4.3
    PostgreSQL - version 42.2.18

Features

List of features ready and TODOs for future development

    Passenger Portal to book launches.
    Administrator Portal to assign pilots.
    Pilot Portal to check flight data.

To-do list:

    Improve UI design for viewing launches.
    Provide immediate feedback on launch data when booked.
    Integrate trajectory analysis 3D modeling.

Getting Started

(include git clone command) (include all environment setup steps)

    git clone https://gitlab.com/210301-java-azure/andrew_shields/launch-p0.git

    cd launch-p0/p0-server

    gradle build

    java -jar build/libs/p0-server-1.0-SNAPSHOT.jar

    open the home.html with your preferred web browser.

Usage

    Start with the login button at the top to sign in, or create a new account.

![Login Image](./images/loginIMG.png)

    Create an account based upon your user role (Admin, Commander, or Passenger)
