package dev.shields.controllers;

import dev.shields.models.User;
import dev.shields.services.UserService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Disabled
    public void testGetUserByIdHandler(){
        Context context = mock(Context.class);
        User user = new User(13, "username", "Commander");
        when(context.pathParam("id")).thenReturn("13");
        when(service.getById(13)).thenReturn(user);

        userController.handleGetUserByIdRequest(context);
        verify(context).json(user);
    }

}
