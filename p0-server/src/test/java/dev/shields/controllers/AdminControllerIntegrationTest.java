package dev.shields.controllers;

import dev.shields.JavalinApp;
import dev.shields.models.User;
import io.javalin.plugin.json.JavalinJson;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AdminControllerIntegrationTest {

    private JavalinApp app = new JavalinApp();

    @Test
    @Disabled
    public void testGetAllUsers(){
        app.start(7000);
        HttpResponse<String> response = Unirest.get("http://localhost:7000/admins/users").asString();

        assertAll(
                ()->assertEquals( 401, response.getStatus()),
                ()->assertEquals( "You are not authorized for this information!", response.getBody()));

        app.stop();
    }

    @Test
    @Disabled
    public void testGetAllUsersAuthorized(){
        app.start(7000);
        HttpResponse<List<User>> response = Unirest.get("http://localhost:7000/admins/users")
                .header("Authorization", "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJTcGFjZVNoaWVsZHMifQ.YH8HSiDEmE2C6MYHttUCx1iHIUudZ-yni_6BsMqFP3I")
                .header("UserId", "SpaceShields")
                .asObject(new GenericType<List<User>>() {});

        assertAll(
                ()->assertEquals( 200, response.getStatus()));
                //()->assertTrue(response.getBody().size>0));

        app.stop();
    }


}
