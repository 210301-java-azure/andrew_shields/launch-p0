package dev.shields.controllers;

import dev.shields.models.User;
import dev.shields.services.AdminService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class AdminControllerTest {

    @InjectMocks
    private AdminController adminController;

    @Mock
    private AdminService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllUsersHandler(){


        Context context = mock(Context.class);
        List<User> users = new ArrayList<>();

        users.add(new User(1, "Uname", "Commander"));
        users.add(new User(2, "Un4me", "Passenger"));
        when(service.getAllUsers()).thenReturn(users);

        adminController.handleGetAllUsersRequest(context);

        verify(context).json(users);

    }

    @Test
    void testPostCommanderHandler(){

        Context context = mock(Context.class);

        when(context.formParam("rocket")).thenReturn("1");
        when(context.formParam("commander")).thenReturn("1");
        when(service.assignCommander(1, 1)).thenReturn(1);

        adminController.handlePostCommander(context);

        verify(context).result("Commander was successfully assigned to rocket: "+1);

    }


}
