package dev.shields.managers;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

class PasswordManagerTest {

    private final PasswordManager passwordManager = new PasswordManagerImpl();

    @Test
    void testPasswordHash() throws InvalidKeySpecException, NoSuchAlgorithmException {
        String password = "solarwind123";

        String hashPass = passwordManager.createHash(password);

        System.out.println(hashPass);

        assert(!hashPass.equals("solarwind123"));

        System.out.println(hashPass.length());

        assert(passwordManager.validatePassword(password, hashPass));
        assert(!passwordManager.validatePassword("omg123", hashPass));

    }

    @Test
    void testValidatePassword(){
        try
        {
            // Print out 10 hashes
            for(int i = 0; i < 10; i++)
                System.out.println(passwordManager.createHash("solarwind123"));

            // Test password validation
            boolean failure = false;
            System.out.println("Running tests...");
            for(int i = 0; i < 100; i++)
            {
                String password = "solarwind123"+i;
                String hash = passwordManager.createHash(password);
                String secondHash = passwordManager.createHash(password);
                if(hash.equals(secondHash)) {
                    System.out.println("FAILURE: TWO HASHES ARE EQUAL!");
                    failure = true;
                }
                String wrongPassword = "omfg123gg"+(i+1);
                if(passwordManager.validatePassword(wrongPassword, hash)) {
                    System.out.println("FAILURE: WRONG PASSWORD ACCEPTED!");
                    failure = true;
                }
                if(!passwordManager.validatePassword(password, hash)) {
                    System.out.println("FAILURE: GOOD PASSWORD NOT ACCEPTED!");
                    failure = true;
                }
            }
            assert(!failure);
        }
        catch(Exception ex)
        {
            System.out.println("ERROR: " + ex);
        }
    }








}
