package dev.shields.data;

import dev.shields.models.LaunchData;
import dev.shields.models.Passenger;
import dev.shields.models.Rocket;

import java.util.List;

public interface PassengerDao {

    List<LaunchData> getAllLaunches();
    Rocket checkAvailableSeats(int id);
    Passenger bookLaunchById(int launchId, int passId);
    Passenger getInfo(String usn);

}
