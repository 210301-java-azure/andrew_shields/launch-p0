package dev.shields.data;

import dev.shields.models.Commander;
import dev.shields.models.LaunchData;
import dev.shields.models.Passenger;
import dev.shields.models.Rocket;

import java.util.List;

public interface CommanderDao {

    LaunchData getLaunchByRocketId(int id);
    LaunchData addNewLaunch(LaunchData launchData);
    Rocket getRocketByCommanderId(int id);
    List<Passenger> getPassengersByLaunchId(int id);
    Commander getCommanderById(int id);

}
