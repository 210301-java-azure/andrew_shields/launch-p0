package dev.shields.data;

import dev.shields.models.*;
import dev.shields.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.NoResultException;


public class UserDaoImp implements UserDao{

    private final Logger logger  = LoggerFactory.getLogger(UserDaoImp.class);

    @Override
    public UserInfo getUserInfoById(int id) {
        try (Session s = HibernateUtil.getSession()){
            logger.info("User "+id+" is getting their client information");
            User user = s.get(User.class, id);
            Query<UserInfo> query = s.createQuery("from UserInfo where client_id = :user", UserInfo.class)
                    .setParameter("user", user);

            return query.getSingleResult();

        }
    }

    @Override
    public User getUserById(int id) {
        try (Session s = HibernateUtil.getSession()){
            logger.info("User "+id+" is getting their information");
            return s.get(User.class, id);
        }
    }

    @Override
    public User getUserByUsername(String usn) {
        try (Session s = HibernateUtil.getSession()){
            Query<User> query = s.createQuery("from User where username = :usn", User.class)
                    .setParameter("usn", usn);

            logger.info("Client "+usn+" is retrieving their data");
            return query.getSingleResult();

        }
    }

    @Override
    public User addNewUser(String usn, String psw, String role) {
        try (Session s = HibernateUtil.getSession()){
            s.beginTransaction();



            // creating a new user object
            User client = new User(usn, psw, role);

            // saving the new user in the database
            int id = (int) s.save(client);

            client.setId(id);

            s.getTransaction().commit();

            return client;




        }

    }

    @Override
    public void addUserInfo(User user, UserInfo userInfo) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();

            userInfo.setUser(user);


            s.save(userInfo);

            tx.commit();

            logger.info("Client "+user.getUsername()+" has updated their client information");
        }
    }

    @Override
    public void deleteUserById(int id) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();

            User client = s.get(User.class, id);

            switch(client.getType()){
                case "Admin":
                    Query<Admin> query = s.createQuery("from Admin where client = :user", Admin.class)
                            .setParameter("user", client);
                    Admin admin = query.getSingleResult();
                    s.delete(admin);
                    break;
                case "Commander":
                    Query<Commander> query1 = s.createQuery("from Commander where client = :user", Commander.class)
                            .setParameter("user", client);
                    Commander commander = query1.getSingleResult();

                    try{
                        Query<Rocket> query2 = s.createQuery("from Rocket where commander = :com", Rocket.class)
                                .setParameter("com", commander);
                        Rocket rocket = query2.getSingleResult();

                        rocket.setCommander(null);

                        s.update(rocket);

                    } catch(NoResultException e){
                        break;
                    }

                    s.delete(commander);
                    break;
                case "Passenger":
                    Query<Passenger> query3 = s.createQuery("from Passenger where client = :user", Passenger.class)
                            .setParameter("user", client);
                    Passenger passenger = query3.getSingleResult();
                    s.delete(passenger);
                    break;
                default:
                    break;
            }


            s.delete(new User(id));
            tx.commit();
        }

    }


    @Override
    public Admin addAdmin(User client, String name, String alloc) {

        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            Admin admin = new Admin(client, name, alloc);
            int id = (int) s.save(admin);
            logger.info("A new administrator has been added to the server");
            admin.setAdminId(id);
            tx.commit();
            return admin;
        }
    }

    @Override
    public Commander addCommander(User user, String name, String launchSite) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            Commander commander = new Commander(user, name, launchSite);
            int id = (int) s.save(commander);
            logger.info("A new commander has been added to the server");
            commander.setId(id);
            tx.commit();
            return commander;
        }
    }

    @Override
    public Passenger addPassenger(User user, String name) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            Passenger passenger = new Passenger(user, name);
            int id = (int) s.save(passenger);
            logger.info("A new passenger has been added to the server");
            passenger.setPassengerId(id);
            tx.commit();
            return passenger;
        }
    }

    @Override
    public void updateInfo(int id, String column, String value) {
        try (Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            User client = s.get(User.class, id);
            Query<UserInfo> query = s.createQuery("from UserInfo where client = :user", UserInfo.class)
                    .setParameter("user",client);
            UserInfo userInfo = query.getSingleResult();
            switch(column){
                case "address":
                    userInfo.setAddress(value);
                    s.save(userInfo);
                    break;
                case "email":
                    userInfo.setEmail(value);
                    s.save(userInfo);
                    break;
                case "phone":
                    int phone = Integer.parseInt(value);
                    userInfo.setPhoneNum(phone);
                    s.save(userInfo);
                    break;
                case "weight":
                    double weight = Double.parseDouble(value);
                    userInfo.setWeight(weight);
                    s.save(userInfo);
                    break;
                case "age":
                    int age = Integer.parseInt(value);
                    userInfo.setAge(age);
                    s.save(userInfo);
                    break;
                default:
                    logger.warn("The column "+column+" cannot be manipulated in client_information");

            }

            logger.info("User "+id+" is updating their "+column+" in client_information to "+value);

            tx.commit();

        }
    }

}
