package dev.shields.data;

import dev.shields.models.*;
import dev.shields.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CommanderDaoImp implements CommanderDao {

    private final Logger logger = LoggerFactory.getLogger(CommanderDaoImp.class);

    @Override
    public LaunchData getLaunchByRocketId(int id) {
        try (Session s = HibernateUtil.getSession()){

            Rocket rocket = s.get(Rocket.class, id);

            try{
                Query<LaunchData> query = s.createQuery("from LaunchData where rocket = :rocket", LaunchData.class)
                        .setParameter("rocket", rocket);
                logger.info("Retrieving launch data assigned to rocket: "+rocket.getRocketName());
                return query.getSingleResult();

            } catch(NoResultException e){
                logger.warn("No launch has been assigned to rocket: "+rocket.getRocketName());
                return null;
            }


        }
    }

    @Override
    public LaunchData addNewLaunch(LaunchData launchData) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();
            int id = (int) s.save(launchData);
            launchData.setLaunchId(id);
            tx.commit();
            logger.info("A new launch was added to the database");
            return launchData;
        }
    }

    @Override
    public Rocket getRocketByCommanderId(int id) {
        try (Session s = HibernateUtil.getSession()){

            Commander commander = s.get(Commander.class, id);

            try{
                Query<Rocket> query = s.createQuery("from Rocket where commander = :com", Rocket.class)
                        .setParameter("com", commander);
                logger.info("Retrieving rocket data assigned to commander: "+commander.getName());
                return query.getSingleResult();

            } catch(NoResultException e){
                logger.warn("No rocket has been assigned to commander: "+commander.getName());
                return null;
            }


        }
    }

    @Override
    public List<Passenger> getPassengersByLaunchId(int id) {
        try(Session s = HibernateUtil.getSession()){
            LaunchData launchData = s.get(LaunchData.class, id);

            CriteriaBuilder cb = s.getCriteriaBuilder();
            CriteriaQuery<Passenger> cq = cb.createQuery(Passenger.class);

            Root<Passenger> root = cq.from(Passenger.class);
            cq.select(root);

            cq.where(cb.equal(root.get("launchId"), launchData));

            Query<Passenger> query = s.createQuery(cq);
            return query.list();

        }
    }

    @Override
    public Commander getCommanderById(int id) {
        try (Session s = HibernateUtil.getSession()){
            return s.get(Commander.class, id);
        }
    }

}

