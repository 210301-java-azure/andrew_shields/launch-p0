package dev.shields.data;

import dev.shields.models.*;

import java.util.List;

public interface AdminDao {

    List<UserInfo> getAllUserInfo();
    List<User> getAllUsers();
    Admin getAdminInfoById(int id);
    List<Admin> getAllAdmins();
    List<Commander> getAllCommanders();
    List<Passenger> getAllPassengers();
    List<LaunchData> getAllLaunches();
    List<Rocket> getAllRockets();
    Rocket addRocket(Rocket rocket);
    int assignCommanderToRocket(int rocketId, int commanderId);
    int assignRocketToLaunch(int launchId, int rocketId);


}
