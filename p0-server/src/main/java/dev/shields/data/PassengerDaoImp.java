package dev.shields.data;

import dev.shields.models.*;
import dev.shields.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class PassengerDaoImp implements PassengerDao{

    private final Logger logger = LoggerFactory.getLogger(PassengerDaoImp.class);

    @Override
    public List<LaunchData> getAllLaunches() {
        try(Session s = HibernateUtil.getSession()){
            List<LaunchData> launchData = s.createQuery("from LaunchData", LaunchData.class).list();

            logger.info("Passenger is accessing all data for available launches");

            return launchData;
        }
    }

    @Override
    public Rocket checkAvailableSeats(int id) {
        try (Session s = HibernateUtil.getSession()){

            return s.get(Rocket.class, id);
        }
    }

    @Override
    public Passenger bookLaunchById(int launchId, int userId) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();

            LaunchData launchData = s.get(LaunchData.class, launchId);

            User user = s.get(User.class, userId);

            Query<Passenger> query = s.createQuery("from Passenger where client = :user", Passenger.class)
                    .setParameter("user", user);

            Passenger passenger = query.getSingleResult();

            passenger.setTravelClass("economy");

            if(launchData.getRocket()==null){
                throw new SecurityException("No rocket has been assigned, cannot book launch at this time!");
            } else {
                Rocket rocket = launchData.getRocket();
                int seatNum = rocket.getSeatsAvail();
                int num = rocket.getSeatsFilled();
                rocket.setSeatsAvail(seatNum-1);
                rocket.setSeatsFilled(num+1);

                String seat = Integer.toString(seatNum);

                passenger.setSeatNo("Z"+seat);

            }

            passenger.setLaunchId(launchData);

            logger.info("Passenger "+passenger.getName()+" has booked a launch to "+launchData.getLaunchDestination());

            s.saveOrUpdate(passenger);

            tx.commit();

            return passenger;

        }
    }

    @Override
    public Passenger getInfo(String usn) {
        try (Session s = HibernateUtil.getSession()){
            Query<User> query = s.createQuery("from User where username = :usn", User.class)
                    .setParameter("usn", usn);

            User user = query.getSingleResult();

            Query<Passenger> query1 = s.createQuery("from Passenger where client = :user", Passenger.class)
                    .setParameter("user", user.getId());

            return query1.getSingleResult();

        }
    }

}
