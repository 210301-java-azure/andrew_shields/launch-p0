package dev.shields.data;

import dev.shields.models.*;


public interface UserDao {

    UserInfo getUserInfoById(int id);
    User getUserById(int id);
    User getUserByUsername(String usn);
    User addNewUser(String usn, String psw, String role);
    void addUserInfo(User user, UserInfo userInfo);
    void deleteUserById(int id);
    Admin addAdmin(User user, String name, String alloc);
    Commander addCommander(User user, String name, String launchSite);
    Passenger addPassenger(User user, String name);
    void updateInfo(int id, String column, String value);

}
