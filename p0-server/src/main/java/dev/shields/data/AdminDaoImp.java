package dev.shields.data;

import dev.shields.models.*;
import dev.shields.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;

public class AdminDaoImp implements AdminDao{

    private final Logger logger = LoggerFactory.getLogger(AdminDaoImp.class);

    @Override
    public List<UserInfo> getAllUserInfo() {
        try (Session s = HibernateUtil.getSession()){

            List<UserInfo> userData = s.createQuery("from UserInfo", UserInfo.class).list();

            logger.info("Accessing all user information in the database - "+userData.size()+" clients retrieved.");

            return userData;
        }
    }

    @Override
    public List<User> getAllUsers() {

        try (Session s = HibernateUtil.getSession()){

            List<User> users = s.createQuery("from User", User.class).list();

            logger.info("Accessing all users in the database - "+users.size()+" clients retrieved.");

            return users;
        }


    }

    @Override
    public Admin getAdminInfoById(int id) {
        try (Session s = HibernateUtil.getSession()){
            logger.info("Admin "+id+" is getting their information");
            return s.get(Admin.class, id);
        }
    }

    @Override
    public List<Admin> getAllAdmins() {
        try (Session s = HibernateUtil.getSession()){

            List<Admin> admins = s.createQuery("from Admin", Admin.class).list();

            logger.info("Accessing all admins in the database - "+admins.size()+" admins retrieved.");

            return admins;
        }
    }

    @Override
    public List<Commander> getAllCommanders() {
        try (Session s = HibernateUtil.getSession()){

            List<Commander> commanders = s.createQuery("from Commander", Commander.class).list();

            logger.info("Accessing all commanders in the database - "+commanders.size()+" commanders retrieved.");

            return commanders;
        }
    }

    @Override
    public List<Passenger> getAllPassengers() {
        try (Session s = HibernateUtil.getSession()){

            List<Passenger> passengers = s.createQuery("from Passenger", Passenger.class).list();

            logger.info("Accessing all passengers in the database - "+passengers.size()+" passengers retrieved.");

            return passengers;
        }
    }

    @Override
    public List<LaunchData> getAllLaunches() {
        try (Session s = HibernateUtil.getSession()){

            List<LaunchData> launches = s.createQuery("from LaunchData", LaunchData.class).list();

            logger.info("Accessing all launch data in the database - "+launches.size()+" launches retrieved.");

            return launches;
        }
    }

    @Override
    public List<Rocket> getAllRockets() {
        try (Session s = HibernateUtil.getSession()){

            List<Rocket> rockets = s.createQuery("from Rocket", Rocket.class).list();

            logger.info("Accessing all rockets in the database - "+rockets.size()+" rockets retrieved.");

            return rockets;
        }
    }

    @Override
    public Rocket addRocket(Rocket rocket) {
        try (Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();

            s.save(rocket);

            if(tx.getStatus().equals(TransactionStatus.ACTIVE)){
                tx.commit();
            }

            logger.info("Admin has added a new rocket: "+rocket.getRocketName());

            return rocket;
        }
    }

    @Override
    public int assignCommanderToRocket(int rocketId, int commandId) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();

            Rocket rocket = s.get(Rocket.class, rocketId);
            Commander commander = s.get(Commander.class, commandId);

            rocket.setCommander(commander);

            s.saveOrUpdate(rocket);

            tx.commit();

            logger.info("Admin has assigned commander "+commander.getName()+" to rocket: "+rocket.getRocketName());

            return rocketId;
        }

    }

    @Override
    public int assignRocketToLaunch(int launchId, int rocketId) {
        try(Session s = HibernateUtil.getSession()){
            Transaction tx = s.beginTransaction();

            Rocket rocket = s.get(Rocket.class, rocketId);
            LaunchData launchData = s.get(LaunchData.class, launchId);

            launchData.setRocket(rocket);

            s.saveOrUpdate(launchData);

            tx.commit();

            logger.info("Admin has assigned rocket "+rocket.getRocketName()+" to a "+launchData.getLaunchDestination()+" mission");

            return launchId;
        }
    }
}
