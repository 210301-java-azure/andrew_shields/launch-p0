package dev.shields.managers;

import dev.shields.models.User;

public interface TokenManager {

    boolean authorize(String token, String username);
    String issueToken(User client);
    boolean checkUser(String token, String usn);

}
