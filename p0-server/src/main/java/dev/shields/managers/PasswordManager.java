package dev.shields.managers;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public interface PasswordManager {

    boolean verify(String username, String password);
    String createHash(String psw) throws NoSuchAlgorithmException, InvalidKeySpecException;
    boolean validatePassword(String pass, String hash) throws NoSuchAlgorithmException, InvalidKeySpecException;

}
