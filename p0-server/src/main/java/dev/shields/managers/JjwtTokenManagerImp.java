package dev.shields.managers;

import dev.shields.models.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;


public class JjwtTokenManagerImp implements TokenManager{

    private final Key key;
    private final Logger logger = LoggerFactory.getLogger(JjwtTokenManagerImp.class);

    @Override
    public boolean authorize(String token, String role) {

        String subject = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getSubject();
        logger.info("Client is being authenticated");

        return subject.equalsIgnoreCase(role);
    }

    @Override
    public String issueToken(User client) {
        String token = Jwts.builder().setSubject(client.getType()).setAudience(client.getUsername()).signWith(key).compact();
        logger.info("Token was generated and is being returned to the client");
        logger.info(token);
        return token;
    }

    @Override
    public boolean checkUser(String token, String username) {
        logger.info(token);
        String usn = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token).getBody().getAudience();

        return usn.equalsIgnoreCase(username);

    }

    public JjwtTokenManagerImp(){

        this.key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
    }

}
