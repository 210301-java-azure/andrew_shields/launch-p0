package dev.shields.services;

import dev.shields.data.PassengerDao;
import dev.shields.data.PassengerDaoImp;
import dev.shields.models.LaunchData;
import dev.shields.models.Passenger;
import dev.shields.models.Rocket;

import java.util.List;

public class PassengerService {

    private PassengerDao passengerDao = new PassengerDaoImp();

    public List<LaunchData> getLaunches(){
        return passengerDao.getAllLaunches();
    }

    public Rocket checkSeatsAvail(int rocketId){
        return passengerDao.checkAvailableSeats(rocketId);
    }

    public Passenger bookLaunch(int launchId, int passId){
        return passengerDao.bookLaunchById(launchId, passId);
    }

    public Passenger getPassengerData(String username){ return passengerDao.getInfo(username); }

}
