package dev.shields.services;

import dev.shields.data.CommanderDao;
import dev.shields.data.CommanderDaoImp;
import dev.shields.models.Commander;
import dev.shields.models.LaunchData;
import dev.shields.models.Passenger;
import dev.shields.models.Rocket;

import java.util.List;

public class CommanderService {

    private CommanderDao commanderDao = new CommanderDaoImp();

    public LaunchData getLaunch(int id){
        return commanderDao.getLaunchByRocketId(id);
    }

    public LaunchData addLaunch(LaunchData lData){
        return commanderDao.addNewLaunch(lData);
    }

    public Rocket getRocket(int commanderId){
        return commanderDao.getRocketByCommanderId(commanderId);
    }

    public List<Passenger> getPassengers(int launchId){
        return commanderDao.getPassengersByLaunchId(launchId);
    }

    public Commander getCommander(int id){
        return commanderDao.getCommanderById(id);
    }

}
