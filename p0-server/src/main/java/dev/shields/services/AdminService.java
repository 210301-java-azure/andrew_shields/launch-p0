package dev.shields.services;

import dev.shields.data.AdminDao;
import dev.shields.data.AdminDaoImp;
import dev.shields.models.*;
import java.util.List;

public class AdminService {

    private AdminDao adminDao = new AdminDaoImp();

    public List<UserInfo> getAllUserInfo(){
        return adminDao.getAllUserInfo();
    }

    public List<User> getAllUsers(){
        return adminDao.getAllUsers();
    }

    public Admin getAdminById(int id){
        return adminDao.getAdminInfoById(id);
    }

    public List<Admin> getAllAdmins(){
        return adminDao.getAllAdmins();
    }

    public List<Commander> getAllCommanders(){
        return adminDao.getAllCommanders();
    }

    public List<Passenger> getAllPassengers(){
        return adminDao.getAllPassengers();
    }

    public List<LaunchData> getAllLaunches(){
        return adminDao.getAllLaunches();
    }

    public List<Rocket> getAllRockets(){
        return adminDao.getAllRockets();
    }

    public Rocket addRocket(Rocket r){
        return adminDao.addRocket(r);
    }

    public int assignCommander(int rocketId, int commanderId){
        return adminDao.assignCommanderToRocket(rocketId, commanderId);
    }

    public int assignRocket(int launchId, int rocketId){
        return adminDao.assignRocketToLaunch(launchId, rocketId);
    }


}
