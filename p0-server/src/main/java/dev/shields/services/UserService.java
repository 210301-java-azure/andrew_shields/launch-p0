package dev.shields.services;

import dev.shields.data.UserDao;
import dev.shields.data.UserDaoImp;
import dev.shields.models.*;

public class UserService {

    private final UserDao userDao = new UserDaoImp();

    public User add(String usn, String psw, String role){ return userDao.addNewUser(usn, psw, role);}

    public User getById(int id){return userDao.getUserById(id);}

    public void deleteUserById(int id){ userDao.deleteUserById(id);}

    public User getByUsername(String usn){ return userDao.getUserByUsername(usn);}

    public Admin addAdmin(User user, String name, String allocation){ return userDao.addAdmin(user, name, allocation);}

    public Commander addCommander(User user, String name, String launchSite){ return userDao.addCommander(user, name, launchSite);}

    public Passenger addPassenger(User user, String name){ return userDao.addPassenger(user, name);}

    public void addUserInfo(User user, UserInfo userInfo){
        userDao.addUserInfo(user, userInfo);
    }

    public UserInfo getUserInfoById(int id){return userDao.getUserInfoById(id);}

    public void updateUserInfo(int id, String column, String value){userDao.updateInfo(id, column, value);}


}
