package dev.shields.controllers;

import com.auth0.jwt.exceptions.JWTCreationException;
import dev.shields.managers.JjwtTokenManagerImp;
import dev.shields.managers.PasswordManager;
import dev.shields.managers.PasswordManagerImpl;
import dev.shields.managers.TokenManager;
import dev.shields.models.User;
import dev.shields.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private final UserService service = new UserService();

    private final TokenManager tokenManager = new JjwtTokenManagerImp();

    private final PasswordManager passwordManager = new PasswordManagerImpl();


    // this method handles POST /login
    public void authenticateLogin(Context ctx){

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        // getting params from a form submission
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        logger.info(user+" attempted to login.");

        try{
            if(user!=null && pass!=null){

                    User client = service.getByUsername(user);

                    if(client!=null){

                        if(passwordManager.verify(user, pass)){
                            String role = client.getType();
                            switch(role){
                                case "Administrator":
                                    logger.info("Admin has logged in.");
                                    ctx.result(tokenManager.issueToken(client));
                                    break;
                                case "Commander":
                                    logger.info("Commander has logged in");
                                    ctx.result(tokenManager.issueToken(client));
                                    break;
                                case "Passenger":
                                    logger.info("Passenger has logged in.");
                                    ctx.result(tokenManager.issueToken(client));
                                    break;
                                default:
                                    logger.warn("Client has logged in without any specified role");
                                    ctx.result("Authorization cannot be given - no role assigned to this user.");
                                    break;
                            }


                        }else {
                            throw new UnauthorizedResponse("Credentials were incorrect");
                        }
                    }

            }else {
                throw new UnauthorizedResponse("Please provide valid credentials");
            }

        }catch (JWTCreationException exception){
            ctx.status(500);
        }

    }

    public void checkAdminAuthentication(Context ctx){

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String token = ctx.header("Authorization");
        String role = "Administrator";

        if(token!=null){
            if(tokenManager.authorize(token, role)){
                logger.info("Admin Authentication was successful");
            }else{
                throw new UnauthorizedResponse();
            }
        } else{
            logger.warn("Admin authorization could not be verified with token provided");
            throw new UnauthorizedResponse("You are not authorized for this information!");
        }

    }

    public void checkCommanderAuthentication(Context ctx){

        if(ctx.method().equals("OPTIONS")){
            return;
        }
        String token = ctx.header("Authorization");
        String role = "Commander";

        if(token!=null){
            if(tokenManager.authorize(token, role)){
                logger.info("Commander Authentication was successful");
            }else{
                throw new UnauthorizedResponse();
            }
        } else{
            logger.warn("Commander authorization could not be verified with token provided");
            throw new UnauthorizedResponse("You are not authorized!");
        }
    }

    public void checkPassengerAuthentication(Context ctx){
        String token = ctx.header("Authorization");
        String role = "Passenger";

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        if(token!=null){
            if(tokenManager.authorize(token, role)){
                logger.info("Passenger Authentication was successful");
            }else{
                throw new UnauthorizedResponse();
            }
        } else{
            logger.warn("Passenger authorization could not be verified with token provided");
            throw new UnauthorizedResponse("Unauthorized!");
        }
    }

    public boolean checkUsername(String token, String usn){
        return tokenManager.checkUser(token, usn);
    }

}
