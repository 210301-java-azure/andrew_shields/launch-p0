package dev.shields.controllers;

import dev.shields.managers.PasswordManager;
import dev.shields.managers.PasswordManagerImpl;
import dev.shields.models.*;
import dev.shields.services.UserService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UserController {


    /* users will have the ability to:
        - get user id and type by username
        - get username and type by id
        - create a new client (with username, password, and user_type)
        - delete all client data with id
        - set client information data in database
        - get client information with id
    */


    Logger logger = LoggerFactory.getLogger(UserController.class); // initialize logging system
    private final UserService service = new UserService();
    private final PasswordManager passwordManager = new PasswordManagerImpl();
    private final AuthController authController = new AuthController();

    // this method handles GET /users (Authorized)
    public void handleGetUserByUsernameRequest(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }

        //String tokenInput = ctx.header("Authorization");
        String usn = ctx.header("Username");

//        if(authController.checkUsername(tokenInput, usn)){
//            User user = service.getByUsername(usn);
//
//            // conditional if-else statement to throw an exception if the id string is null or return user info
//            if(user==null){
//                logger.warn("Failed to find user with username: "+usn);
//                throw new NotFoundResponse("No user found with the Username: "+usn);
//            } else{
//                ctx.json(user);
//            }
//        } else{
//            throw new BadRequestResponse();
//        }

        User user = service.getByUsername(usn);

        // conditional if-else statement to throw an exception if the id string is null or return user info
        if(user==null){
            logger.warn("Failed to find user with username: "+usn);
            throw new NotFoundResponse("No user found with the Username: "+usn);
        } else{
            ctx.json(user);
        }


    }

    // this method handles GET /users/:id
    public void handleGetUserByIdRequest(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String idInput = ctx.pathParam("id"); // storing the string parameter into a local variable
        int id = Integer.parseInt(idInput);
        User user = service.getById(id); // using the .getById method in the service module to find specific user and store into local variable

        // conditional if-else statement to throw an exception if the id string is null or return user info
        if(user==null){
            logger.warn("Failed to find user with ID: "+idInput);
            throw new NotFoundResponse("No user found with the User ID: "+idInput);
        } else{
            ctx.json(user);
        }

    }

    // this method handles POST /users through form parameters
    public void handlePostNewUser(Context ctx) throws InvalidKeySpecException, NoSuchAlgorithmException {

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        Map<String, String> stringMap = new HashMap<>();
        String input = ctx.body();
        logger.info("input: "+input);
        String values = input.substring(1, input.length()-1);
        String[] pairs = values.split(",");

        for(String x : pairs) {
            String[] keys = x.split(":");
            stringMap.put(keys[0].substring(1,keys[0].length()-1), keys[1].substring(1,keys[1].length()-1));
            logger.info("key: "+keys[0]+" value: "+keys[1]);
        }


        String usn = stringMap.get("username"); // create local variable for the username from form parameter
        String role = stringMap.get("role"); // create local variable for user_type


        String psw = stringMap.get("password"); // local storage for password variable

        // hashing the password
        String hashPass = passwordManager.createHash(psw);

        logger.info(hashPass);

        logger.info("Password for user "+usn+" has been hashed");

        if(hashPass.equals(psw)) {
            // making sure the password hash was successful
            throw new SecurityException("Password was not hashed successfully");
        }

        logger.info("Adding a new user: "+usn); // logging the addition of a new user
        User client = service.add(usn, hashPass, role);
        logger.info("Client account was made for user: "+usn);

        // switch statement for roles to create database records from context in proper tables
        switch(Objects.requireNonNull(role)){
            case "Administrator":
                logger.info("The client "+usn+" is an administrator.");
                String name = stringMap.get("name");
                String allocation = stringMap.get("allocation");
                Admin admin = service.addAdmin(client, name, allocation);
                logger.info("The administrator"+name+" has been added to the admin database.");
                ctx.json(admin);
                break;
            case "Commander":
                logger.info("The new client "+usn+" is a commander.");
                String cName = stringMap.get("name");
                String launchSite = stringMap.get("launch_site");
                Commander commander = service.addCommander(client, cName, launchSite);
                logger.info("The commander "+commander.getName()+" was added to the commander database.");
                ctx.json(commander);
                break;
            case "Passenger":
                logger.info("The new client "+usn+" is a passenger.");
                String pName = stringMap.get("name");
                Passenger passenger = service.addPassenger(client, pName);
                logger.info("The Passenger "+passenger.getName()+" was added to the passenger database.");
                ctx.json(passenger);
                break;
            default:
                logger.warn("The client "+usn+" did not specify a role");
                break;

        }
        ctx.json(client);

    }


    public void handleDeleteUserById(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String idInput = ctx.pathParam("id"); // turning the path parameter into a string to find user info

        int id = Integer.parseInt(idInput);

        // conditional if-else statement to ensure the id input is not an empty string
        if (id>0){
            service.deleteUserById(id);
            logger.info("User "+idInput+" has been removed from the database.");
        }else{
            throw new BadRequestResponse("This form of input is forbidden!");
        }

        ctx.result("You have successfully removed your account with ID: "+idInput);

    }

    // this method handles POST /users/info/:id
    public void handlePostUserInfoRequest(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }

        String idInput = ctx.pathParam("id");
        int id = Integer.parseInt(idInput);

        UserInfo userInfo = ctx.bodyAsClass(UserInfo.class);

//        String name = ctx.formParam("name");
//        String email = ctx.formParam("email");
//        String address = ctx.formParam("address");
//        String stringPhone = ctx.formParam("phone number");
//        int phone = Integer.parseInt(stringPhone);
//        String stringWeight = ctx.formParam("weight");
//        double weight = Double.parseDouble(stringWeight);
//        String stringAge = ctx.formParam("age");
//        int age = Integer.parseInt(stringAge);

        User client = service.getById(id);

        if (id>0){
            service.addUserInfo(client, userInfo);
            logger.info("User information for "+client.getUsername()+" has been added from the database.");
        }else{
            throw new BadRequestResponse("This form of input is forbidden!");
        }

    }

    // this method handles GET /users/info/:id
    public void handleGetUserInfoByIdRequest(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }
        String idInput = ctx.pathParam("id");
        int id = Integer.parseInt(idInput);

        if (id>0){
            UserInfo userInfo = service.getUserInfoById(id);
            logger.info("User "+idInput+" has retrieved information from the database.");
            ctx.json(userInfo);
        }else{
            throw new BadRequestResponse("Request failed!");
        }

    }

    // this method handles PUT /users/info/:id
    public void handlePutUserInfoUpdate(Context ctx){
        if(ctx.method().equals("OPTIONS")){
            return;
        }
        String idInput = ctx.pathParam("id");
        String column = ctx.formParam("column");
        String change = ctx.formParam("new value");

        if(idInput.matches("^\\d+$")) {
            int id = Integer.parseInt(idInput);
            logger.info("User "+id+" is updating "+column+" in client_information");
            service.updateUserInfo(id, column, change);
            ctx.result("User: "+id+" -- Your information has been updated.");

        }else{
            throw new BadRequestResponse("This form of input is forbidden!");
        }
    }



}
