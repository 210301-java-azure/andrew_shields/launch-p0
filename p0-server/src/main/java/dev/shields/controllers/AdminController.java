package dev.shields.controllers;

import dev.shields.models.Rocket;
import dev.shields.services.AdminService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;


public class AdminController {

    Logger logger = LoggerFactory.getLogger(AdminController.class);
    private AdminService service = new AdminService();

    // this method handles GET /admins/admins
    public void handleGetAllAdminsRequest(Context ctx){

        logger.info("Admin accessing full list of administrators");
        ctx.json(service.getAllAdmins());

    }

    // this method handles GET /admins/users/info
    public void handleGetAllUserInfoRequest(Context ctx){
        logger.info("Admin accessing all user information in database");
        ctx.json(service.getAllUserInfo());
    }

    // this method handles GET /admins/users
    public void handleGetAllUsersRequest(Context ctx){
        logger.info("Administrator accessing list of all users");
        ctx.json(service.getAllUsers());
    }

    // this method handles GET /admins/:id
    public void handleGetAdminByIdRequest(Context ctx){

        String idString = ctx.pathParam("id");
        int idInput = Integer.parseInt(idString);

        logger.info("Retrieving admin info from database with id: "+idString);
        ctx.json(service.getAdminById(idInput));

    }

    // this method handles GET /admins/commanders
    public void handleGetAllCommandersRequest(Context ctx){
        logger.info("Admin accessing full list of Commanders from database");
        ctx.json(service.getAllCommanders());
    }

    // this method handles GET /admins/passengers
    public void handleGetAllPassengersRequest(Context ctx){
        logger.info("Admin accessing full list of Passengers from database");
        ctx.json(service.getAllPassengers());
    }

    // this method handles GET /admins/launches
    public void handleGetAllLaunchesRequest(Context ctx){
        logger.info("Admin accessing full list of launches from database");
        ctx.json(service.getAllLaunches());
    }

    // this method handles GET /admins/rockets
    public void handleGetAllRocketsRequest(Context ctx){
        logger.info("Admin accessing full list of rockets from database");
        ctx.json(service.getAllRockets());
    }

    // this method handles POST /admins/rockets
    public void handlePostNewRocket(Context ctx){

        if(ctx.method().equals("OPTIONS")){
            return;
        }

        Rocket rocket = ctx.bodyAsClass(Rocket.class);
        logger.info("Admin adding a new rocket to database");
        service.addRocket(rocket);
        ctx.status(201);

    }

    // this method handles POST /admins/rockets/commander
    public void handlePostCommander(Context ctx){

        String sRocketId = ctx.formParam("rocket");
        String sCommandId = ctx.formParam("commander");
        if(Objects.equals(sRocketId, "") || Objects.equals(sCommandId, "")){
            throw new BadRequestResponse("Input ("+sRocketId+", "+sCommandId+") could not be parsed to an int.");
        }else{
            assert sRocketId != null;
            int rocketId = Integer.parseInt(sRocketId);
            assert sCommandId != null;
            int commandId = Integer.parseInt(sCommandId);
            if (rocketId>0 && commandId>0){
                logger.info("Admin assigning commander to rocket");
                int id = service.assignCommander(rocketId, commandId);
                ctx.result("Commander was successfully assigned to rocket: "+id);
            }
        }
    }

    // this method handles POST /admins/launches/rocket
    public void handlePostRocket(Context ctx){

        String sRocketId = ctx.formParam("rocket");
        String sLaunchId = ctx.formParam("launch");
        if(Objects.equals(sRocketId, "") || Objects.equals(sLaunchId, "")){
            throw new BadRequestResponse("Input ("+sRocketId+", "+sLaunchId+") could not be parsed to an int.");
        }else{
            assert sRocketId != null;
            int rocketId = Integer.parseInt(sRocketId);
            assert sLaunchId != null;
            int launchId = Integer.parseInt(sLaunchId);
            if (rocketId>0 && launchId>0){
                logger.info("Admin assigning rocket to launch");
                service.assignRocket(launchId, rocketId);
                ctx.result("Rocket was successfully assigned to launch: "+launchId);
            }
        }
    }


    public void handleDeleteAdminById(Context ctx){
//        String stringId = ctx.pathParam("id");
//        if(stringId.matches("^\\d+$")){
//            int idInput = Integer.parseInt(stringId);
//            logger.info("removing an item with id: "+idInput);
//            adminData.deleteAdminById(idInput);
//            ctx.result("You have successfully deleted the admin with ID: "+idInput);
//        }else{
//            throw new BadRequestResponse("Input"+stringId+"could not be parsed to an int.");
//        }

    }


}
