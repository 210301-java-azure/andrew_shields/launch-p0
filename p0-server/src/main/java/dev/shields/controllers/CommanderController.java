package dev.shields.controllers;

import dev.shields.models.Commander;
import dev.shields.models.LaunchData;
import dev.shields.models.Passenger;
import dev.shields.models.Rocket;
import dev.shields.services.CommanderService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CommanderController {


    private Logger logger = LoggerFactory.getLogger(CommanderController.class);
    private CommanderService service = new CommanderService();


    // this method handles GET commanders/launches/:id
    public void handleGetLaunchByIdRequest(Context ctx){

        String launchId = ctx.pathParam("id");

        if(launchId.matches("^\\d+$")){
            int idInput = Integer.parseInt(launchId);
            logger.info("Commander accessing launch data for rocket with id: "+idInput);
            LaunchData launchData = service.getLaunch(idInput);

            if(launchData==null){
                logger.info("No launch found with rocket ID: "+idInput);
                ctx.result("There are no launches assigned to rocket "+idInput);
            }else{
                ctx.json(launchData);
            }

        }else {
            throw new BadRequestResponse("Input" + launchId + "could not be parsed to an int.");
        }

    }

    // this method handles POST /commanders/launches
    public void handlePostLaunchRequest(Context ctx){

        LaunchData launch = ctx.bodyAsClass(LaunchData.class);

        logger.info("Commander is adding a new launch to the database");

        service.addLaunch(launch);

        if(launch==null){
            logger.info("The new launch did not get stored in the database");
            ctx.result("Adding launch to database was unsuccessful.");
        }else{
            ctx.status(201);
        }

    }

    // this method handles GET /commanders/:id/rocket
    public void handleGetRocketByPilotIdRequest(Context ctx){

        String cmdId = ctx.pathParam("id");
        if(cmdId.matches("^\\d+$")){
            int pilotId = Integer.parseInt(cmdId);
            logger.info("Pilot, "+pilotId+", is accessing their assigned rocket information");
            Rocket rocket = service.getRocket(pilotId);

            if(rocket==null){
                logger.info("Commander "+pilotId+" has not been assigned a rocket");
                ctx.result("You have not been assigned to a rocket by an admin");
            } else{
                ctx.json(rocket);
            }

        }else{
            throw new BadRequestResponse("Input"+cmdId+"could not be parsed to an int.");
        }

    }

    // this method handles GET /commanders/launches/:id/passengers
    public void handleGetPassengersRequest(Context ctx){

        String id = ctx.pathParam("id");
        if(id.matches("^\\d+$")){
            int launchId = Integer.parseInt(id);
            logger.info("Commander accessing all passengers booked to launch: "+launchId);
            List<Passenger> passengers = service.getPassengers(launchId);
            if (passengers==null){
                ctx.result("No Passengers have booked this launch");
            }else{
                ctx.json(passengers);
            }
        }else{
            throw new BadRequestResponse("Input"+id+"could not be parsed to an int.");
        }

    }

    // this method handles GET /commanders/:id
    public void handleGetCommanderRequest(Context ctx){

        String id = ctx.pathParam("id");
        if(id.matches("^\\d+$")){
            int pilotId = Integer.parseInt(id);
            logger.info("Commander "+pilotId+" accessing their stored information");
            Commander commander = service.getCommander(pilotId);

            if(commander==null){
                ctx.result("No commander found for this ID: "+pilotId);
            } else{
                ctx.json(commander);
            }

        }else{
            throw new BadRequestResponse("Input"+id+"could not be parsed to an int.");
        }

    }


}
