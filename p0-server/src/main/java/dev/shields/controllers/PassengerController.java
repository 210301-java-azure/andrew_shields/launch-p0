package dev.shields.controllers;

import dev.shields.models.Passenger;
import dev.shields.models.Rocket;
import dev.shields.services.PassengerService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PassengerController {

    private Logger logger = LoggerFactory.getLogger(PassengerController.class);
    private PassengerService service = new PassengerService();

    // this method handles GET /passengers/launches
    public void handleGetLaunchDataRequest(Context ctx){

        logger.info("passenger accessing full list of available launches");
        ctx.json(service.getLaunches());

    }

    // this method handles GET /passengers/launches/:id
    public void handleGetAvailableSeatsRequest(Context ctx){

        String idInput = ctx.pathParam("id");

        int id = Integer.parseInt(idInput);

        logger.info("passenger is checking available seats for launch: "+id);

        Rocket rocket = service.checkSeatsAvail(id);

        ctx.result("The number of available seats for launch "+id+" is: "+rocket.getSeatsAvail());
    }

    // this method handles POST /passengers/:id
    public void handlePostBookLaunchById(Context ctx){

        String sLaunchId = ctx.formParam("launch");
        String sClientId = ctx.pathParam("id");

        int launchId = Integer.parseInt(sLaunchId);
        int clientId = Integer.parseInt(sClientId);

        ctx.json(service.bookLaunch(launchId, clientId));

        logger.info("Client, "+clientId+", has been successfully booked to launch: "+launchId);

    }

    public void handleGetPassengerInfo(Context ctx){

        String usn = ctx.pathParam("id");

        logger.info("Passenger retrieving data for username: "+usn);

        Passenger passenger = service.getPassengerData(usn);

        if(passenger==null){
            logger.info("No passenger connected to the username: "+usn);
            ctx.result("Passenger could not be found under this username: "+usn);
        }else{
            ctx.json(passenger);
        }

    }

}
