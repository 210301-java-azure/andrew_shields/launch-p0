package dev.shields.models;

import javax.persistence.*;

@Entity
@Table(name = "launch_data")
public class LaunchData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "launch_id")
    private int launchId;

    @Column(name = "launch_date")
    private String launchDate;

    @Column(name = "launch_time")
    private String launchTime;

    @Column(name = "location")
    private String launchLocation;

    @Column(name = "destination")
    private String launchDestination;

    @OneToOne
    @JoinColumn(name = "rocket_id")
    private Rocket rocket;

    @Column(name = "arrival_date")
    private String arrivalDate;

    @Column(name = "arrival_time")
    private String arrivalTime;

    public int getLaunchId() {
        return launchId;
    }

    public void setLaunchId(int launchId) {
        this.launchId = launchId;
    }

    public String getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(String launchDate) {
        this.launchDate = launchDate;
    }

    public String getLaunchTime() {
        return launchTime;
    }

    public void setLaunchTime(String launchTime) {
        this.launchTime = launchTime;
    }

    public String getLaunchLocation() {
        return launchLocation;
    }

    public void setLaunchLocation(String launchLocation) {
        this.launchLocation = launchLocation;
    }

    public String getLaunchDestination() {
        return launchDestination;
    }

    public void setLaunchDestination(String launchDestination) {
        this.launchDestination = launchDestination;
    }

    public Rocket getRocket() {
        return rocket;
    }

    public void setRocket(Rocket rocket) {
        this.rocket = rocket;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public LaunchData(){
        super();
    }

    public LaunchData(String launchDate, String launchTime, String launchLocation, String launchDestination, String arrivalDate, String arrivalTime){
        super();
        setLaunchDate(launchDate);
        setLaunchTime(launchTime);
        setLaunchLocation(launchLocation);
        setLaunchDestination(launchDestination);
        setArrivalDate(arrivalDate);
        setArrivalTime(arrivalTime);
    }

    public LaunchData(int id, String launchDate, String launchTime, String launchLocation, String launchDestination, String arrivalDate, String arrivalTime){
        super();
        setLaunchId(id);
        setLaunchDate(launchDate);
        setLaunchTime(launchTime);
        setLaunchLocation(launchLocation);
        setLaunchDestination(launchDestination);
        setArrivalDate(arrivalDate);
        setArrivalTime(arrivalTime);
    }

    public LaunchData(String launchDate, String launchTime, String launchLocation, String launchDestination,Rocket rocket, String arrivalDate, String arrivalTime){
        super();
        setLaunchDate(launchDate);
        setLaunchTime(launchTime);
        setLaunchLocation(launchLocation);
        setLaunchDestination(launchDestination);
        setRocket(rocket);
        setArrivalDate(arrivalDate);
        setArrivalTime(arrivalTime);
    }

    public LaunchData(int id, String launchDate, String launchTime, String launchLocation, String launchDestination,Rocket rocket, String arrivalDate, String arrivalTime){
        super();
        setLaunchId(id);
        setLaunchDate(launchDate);
        setLaunchTime(launchTime);
        setLaunchLocation(launchLocation);
        setLaunchDestination(launchDestination);
        setRocket(rocket);
        setArrivalDate(arrivalDate);
        setArrivalTime(arrivalTime);
    }

    @Override
    public String toString() {
        return "LaunchData{" +
                "launchId=" + launchId +
                ", launchDate='" + launchDate + '\'' +
                ", launchTime='" + launchTime + '\'' +
                ", launchLocation='" + launchLocation + '\'' +
                ", launchDestination='" + launchDestination + '\'' +
                ", rocket=" + rocket +
                ", arrivalDate='" + arrivalDate + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                '}';
    }
}
