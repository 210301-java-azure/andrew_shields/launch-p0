package dev.shields.models;

import javax.persistence.*;

@Entity
@Table(name = "administrator")
public class Admin{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_id")
    private int adminId;

    @Column(name = "name")
    private String name;

    @Column(name = "allocation")
    private String allocation;

    @OneToOne
    @JoinColumn(name = "client_id")
    private User client;

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public Admin(){
        super();
    }

    public String getAllocation() {
        return allocation;
    }

    public void setAllocation(String allocation) {
        this.allocation = allocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Admin(User user, String name, String allocation){
        super();
        setClient(user);
        setAllocation(allocation);
        setName(name);
    }


    public Admin(User user, int id, String name, String allocation){
        super();
        setAdminId(id);
        setAllocation(allocation);
        setClient(user);
        setName(name);
    }

    @Override
    public String toString() {
        return "Admin{" +
                "adminId=" + adminId +
                ", name='" + name + '\'' +
                ", allocation='" + allocation + '\'' +
                ", client=" + client +
                '}';
    }
}
