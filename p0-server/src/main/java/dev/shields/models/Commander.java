package dev.shields.models;

import javax.persistence.*;

@Entity
@Table(name = "commander")
public class Commander {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "commander_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "launch_site")
    private String launchSite;

    @OneToOne
    @JoinColumn(name = "client_id")
    private User client;

    @Override
    public String toString() {
        return "Commander{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", launchSite='" + launchSite + '\'' +
                ", client=" + client +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLaunchSite() {
        return launchSite;
    }

    public void setLaunchSite(String launchSite) {
        this.launchSite = launchSite;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public Commander(){
        super();
    }

    public Commander(User clientId, String name, String launchSite){
        super();
        setClient(clientId);
        setName(name);
        setLaunchSite(launchSite);
    }

    public Commander(User clientId, int id, String name, String launchSite){
        super();
        setClient(clientId);
        setId(id);
        setName(name);
        setLaunchSite(launchSite);
    }

}
