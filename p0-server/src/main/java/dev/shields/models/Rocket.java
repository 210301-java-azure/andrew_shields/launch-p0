package dev.shields.models;

import javax.persistence.*;

@Entity
@Table(name = "rocket", uniqueConstraints = {@UniqueConstraint(columnNames = "rocket_name")})
public class Rocket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rocket_id")
    private int rocketId;

    @Column(name = "rocket_name")
    private String rocketName;

    @Column(name = "seat_count")
    private int seatCount;

    @Column(name = "seats_available")
    private int seatsAvail;

    @Column(name = "seats_filled")
    private int seatsFilled;

    @OneToOne
    @JoinColumn(name = "commander_id")
    private Commander commander;

    public int getRocketId() {
        return rocketId;
    }

    public void setRocketId(int rocketId) {
        this.rocketId = rocketId;
    }

    public String getRocketName() {
        return rocketName;
    }

    public void setRocketName(String rocketName) {
        this.rocketName = rocketName;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public int getSeatsAvail() {
        return seatsAvail;
    }

    public void setSeatsAvail(int seatsAvail) {
        this.seatsAvail = seatsAvail;
    }

    public int getSeatsFilled() {
        return seatsFilled;
    }

    public void setSeatsFilled(int seatsFilled) {
        this.seatsFilled = seatsFilled;
    }

    public Commander getCommander() {
        return commander;
    }

    public void setCommander(Commander commander) {
        this.commander = commander;
    }

    public Rocket(){
        super();
    }

    public Rocket(String name, int seatCount, int seatsAvail, int seatsFilled){
        super();
        setRocketName(name);
        setSeatCount(seatCount);
        setSeatsAvail(seatsAvail);
        setSeatsFilled(seatsFilled);
    }

    public Rocket(String name, int seatCount, int seatsAvail, int seatsFilled, Commander commander){
        super();
        setRocketName(name);
        setSeatCount(seatCount);
        setSeatsAvail(seatsAvail);
        setSeatsFilled(seatsFilled);
        setCommander(commander);
    }

    public Rocket(int id, String name, int seatCount, int seatsAvail, int seatsFilled){
        super();
        setRocketId(id);
        setRocketName(name);
        setSeatCount(seatCount);
        setSeatsAvail(seatsAvail);
        setSeatsFilled(seatsFilled);
    }

    public Rocket(int id, String name, int seatCount, int seatsAvail, int seatsFilled, Commander commander){
        super();
        setRocketId(id);
        setRocketName(name);
        setSeatCount(seatCount);
        setSeatsAvail(seatsAvail);
        setSeatsFilled(seatsFilled);
        setCommander(commander);
    }

    @Override
    public String toString() {
        return "Rocket{" +
                "rocketId=" + rocketId +
                ", rocketName='" + rocketName + '\'' +
                ", seatCount=" + seatCount +
                ", seatsAvail=" + seatsAvail +
                ", seatsFilled=" + seatsFilled +
                ", commander=" + commander +
                '}';
    }
}
