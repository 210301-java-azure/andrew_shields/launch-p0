package dev.shields.models;


import javax.persistence.*;

@Entity
@Table(name = "passenger")
public class Passenger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "passenger_id")
    private int passengerId;

    @Column(name = "name")
    private String name;

    @Column(name = "seat_number")
    private String seatNo;

    @Column(name = "travel_class")
    private String travelClass;

    @OneToOne
    @JoinColumn(name = "launch_id")
    private LaunchData launchId;

    @OneToOne
    @JoinColumn(name = "client_id")
    private User client;

    public int getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(int passengerId) {
        this.passengerId = passengerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public LaunchData getLaunchId() {

        return launchId;
    }

    public void setLaunchId(LaunchData launchId) {
        this.launchId = launchId;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public Passenger(){
        super();
    }

    public Passenger(User user, String name){
        super();
        setClient(user);
        setName(name);
    }


    public Passenger(User client, int id, String name){
        super();
        setClient(client);
        setPassengerId(id);
        setName(name);
    }
    public Passenger(User client, int id, String name, String seatNo, String travelClass){
        super();
        setSeatNo(seatNo);
        setTravelClass(travelClass);
        setClient(client);
        setPassengerId(id);
        setName(name);
    }

    public Passenger(int id, String seatNo, String travelClass, LaunchData launchId){
        super();
        setLaunchId(launchId);
        setPassengerId(id);
        setTravelClass(travelClass);
        setSeatNo(seatNo);
    }
    public Passenger(int id, String name, String seatNo, String travelClass, LaunchData launchId){
        super();
        setName(name);
        setLaunchId(launchId);
        setPassengerId(id);
        setTravelClass(travelClass);
        setSeatNo(seatNo);
    }

    public Passenger(User client, int id, String seatNo, String travelClass, LaunchData launchId){
        super();
        setClient(client);
        setPassengerId(id);
        setSeatNo(seatNo);
        setTravelClass(travelClass);
        setLaunchId(launchId);
    }

    public Passenger(User client, int id, String name, String seatNo, String travelClass, LaunchData launchId){
        super();
        setName(name);
        setClient(client);
        setPassengerId(id);
        setSeatNo(seatNo);
        setTravelClass(travelClass);
        setLaunchId(launchId);
    }


    @Override
    public String toString() {
        return "Passenger{" +
                "name='" + name + '\'' +
                ", seatNo='" + seatNo + '\'' +
                ", travelClass='" + travelClass + '\'' +
                ", launchId=" + launchId +
                ", passengerId=" + passengerId +
                ", client=" + client +
                '}';
    }
}
