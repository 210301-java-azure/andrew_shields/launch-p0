package dev.shields.models;

import javax.persistence.*;

@Entity
@Table(name = "client", uniqueConstraints = {@UniqueConstraint(columnNames = "username")})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private int id;

    @Column(name = "username")
    private String username;

    @Column(name = "role")
    private String type;



    @Column(name = "password")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userId) {
        this.username = userId;
    }

    public User(){
        super();
    }

    public User(int id){ this.id = id; }

    public User(String usn, String role){
        super();
        setUsername(usn);
        setType(role);
    }
    public User(String usn, String psw, String role){
        super();
        setUsername(usn);
        setPassword(psw);
        setType(role);
    }

    public User(int id, String userId, String type){
        super();
        setId(id);
        setUsername(userId);
        setType(type);
    }

    public User(int id, String userId, String password, String type){
        super();
        setId(id);
        setUsername(userId);
        setPassword(password);
        setType(type);
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", userId='" + username + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}

