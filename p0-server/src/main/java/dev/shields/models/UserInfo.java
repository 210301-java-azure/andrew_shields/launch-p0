package dev.shields.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "client_information")
public class UserInfo implements Serializable {

    @Column(name = "full_name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_number")
    private int phoneNum;

    @Column(name = "weight")
    private double weight;

    @Column(name = "age")
    private int age;

    @Id
    @OneToOne
    @JoinColumn(name = "client_id")
    private User user;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public UserInfo(){
        super();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", phoneNum=" + phoneNum +
                ", weight=" + weight +
                ", age=" + age +
                ", user=" + user +
                '}';
    }

    public UserInfo(String name, String email, String address, int phone, double weight, int age){
        super();
        setName(name);
        setEmail(email);
        setAddress(address);
        setPhoneNum(phone);
        setWeight(weight);
        setAge(age);
    }

    public UserInfo(User user, String name, String email, String address, int phone, double weight, int age){
        super();
        setUser(user);
        setName(name);
        setEmail(email);
        setAddress(address);
        setPhoneNum(phone);
        setWeight(weight);
        setAge(age);
    }

}
