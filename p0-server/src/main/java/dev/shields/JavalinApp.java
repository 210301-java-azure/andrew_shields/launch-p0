package dev.shields;

import dev.shields.controllers.*;
import dev.shields.util.SecurityUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {



    UserController userController = new UserController();
    AdminController adminController = new AdminController();
    AuthController authController = new AuthController();
    PassengerController passengerController = new PassengerController();
    CommanderController commanderController = new CommanderController();
    SecurityUtil securityUtil = new SecurityUtil();

    Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins).routes(()->{

        path("/", ()->{
            before("admins", authController::checkAdminAuthentication);
            before("admins/*",authController::checkAdminAuthentication);
            before("commanders", authController::checkCommanderAuthentication);
            before("commanders/*", authController::checkCommanderAuthentication);
        });

        path("login", ()->{
            after("/", securityUtil::attachResponseHeaders);
            post(authController::authenticateLogin); // allows a client to log in and receive a token
        });

        path("users", ()->{
            get(userController::handleGetUserByUsernameRequest);
            post(userController::handlePostNewUser);
            path("info", ()->{
                path(":id", ()->{
                    post(userController::handlePostUserInfoRequest);
                    get(userController::handleGetUserInfoByIdRequest);
                    put(userController::handlePutUserInfoUpdate);
                });
            });
            path(":id", () ->{
                get(userController::handleGetUserByIdRequest);
                delete(userController::handleDeleteUserById);
            });
        });

        path("admins", ()->{
            get(authController::checkAdminAuthentication);
            path("admins", ()->{
                get(adminController::handleGetAllAdminsRequest);
            });
            path("users",()->{
                get(adminController::handleGetAllUsersRequest);
                path("info", ()->{
                    get(adminController::handleGetAllUserInfoRequest);
                });
            });
            path("commanders", ()->{
                get(adminController::handleGetAllCommandersRequest);
            });
            path("passengers", ()->{
                get(adminController::handleGetAllPassengersRequest);
            });
            path("rockets", ()->{
                get(adminController::handleGetAllRocketsRequest);
                post(adminController::handlePostNewRocket);
                path("commander", ()->{
                    post(adminController::handlePostCommander);
                });
            });
            path("launches", ()->{
                get(adminController::handleGetAllLaunchesRequest);
                path("rocket", ()->{
                    post(adminController::handlePostRocket);
                });
            });
            path(":id", ()->{
                get(adminController::handleGetAdminByIdRequest);
            });
        });

        path("passengers", ()->{
            path("launches", ()->{
                get(passengerController::handleGetLaunchDataRequest);
                path(":id", ()->{
                    get(passengerController::handleGetAvailableSeatsRequest);
                });
            });
            path(":id", ()->{
                get(passengerController::handleGetPassengerInfo);
                post(passengerController::handlePostBookLaunchById);
            });
        });

        path("commanders", ()->{
            path("launches",()->{
                post(commanderController::handlePostLaunchRequest);
                path(":id",()->{
                    get(commanderController::handleGetLaunchByIdRequest);
                    path("passengers",()->{
                        get(commanderController::handleGetPassengersRequest);
                    });
                });

            });
            path(":id", ()->{
                get(commanderController::handleGetCommanderRequest);
                path("rocket",()->{
                    get(commanderController::handleGetRocketByPilotIdRequest);
                });
            });

        });

    });


    public void start(int portId){
        this.app.start(portId);
    }

    public void stop(){
        this.app.stop();
    }

}
