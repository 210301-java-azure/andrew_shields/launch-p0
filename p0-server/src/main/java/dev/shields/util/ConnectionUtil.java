package dev.shields.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    public static Connection getConnection() throws SQLException {

        if (connection == null || connection.isClosed()) {
            String connectionUrl = System.getenv("TDBconnectionUrl");
            String username = System.getenv("TDBusername");
            String password = System.getenv("TDBpassword");


            connection = DriverManager.getConnection(connectionUrl, username, password);
        }
        return connection;
    }



}
