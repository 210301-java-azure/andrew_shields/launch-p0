package dev.shields.util;

import dev.shields.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration = new Configuration();

            Properties settings = new Properties();
            settings.put(Environment.URL, System.getenv("DBconnectionUrl"));
            settings.put(Environment.USER, System.getenv("DBusername"));
            settings.put(Environment.PASS, System.getenv("DBpassword"));

            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "validate");
            settings.put(Environment.SHOW_SQL, "false");

            configuration.setProperties(settings);

            // provide mapping
            configuration.addAnnotatedClass(User.class);
            configuration.addAnnotatedClass(Admin.class);
            configuration.addAnnotatedClass(Commander.class);
            configuration.addAnnotatedClass(Passenger.class);
            configuration.addAnnotatedClass(LaunchData.class);
            configuration.addAnnotatedClass(Rocket.class);
            configuration.addAnnotatedClass(UserInfo.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }

    // closes caches and connection pools
    public static void shutdown(){ getSessionFactory().close(); }
}
